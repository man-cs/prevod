import sys

text = """\
prevod -- Nástroj pro převod formátu manuálových stránek z man-cs
          do jiného formátu

Použití (z adresáře nadřazeného adresáři prevod):
    python prevod [VOLBY] --format=FORMÁT vstup > výstup

Volby:
    -f FORMÁT, --format=FORMÁT
        Nastaví formát výstupu

    --formats, --formaty
        Vypíše dostupné formáty

    --encoding=ZNAKOVÁ_SADA, --charset=ZNAKOVÁ_SADA, --kodovani=ZNAKOVÁ_SADA
        Zvolí název kódování, který je uložen ve výstupu (např. <meta> hlavička
        v HTML). Neovlivňuje použité kódování vstupu a výstupu.

    --css=NÁZEV
        Pokud to formát výstupu dovoluje, vloží CSS styl do dokumentu

    --seznam-css
        Vypíše dostupné CSS styly

    --help
        Zobrazí tuto nápovědu
"""

formaty = ["html"]

def zobraz():
    sys.stdout.write(text)
