styly = {
    "zakladni": """
        .odsazeny {
            margin-left: 1cm;
        }
        
        .shell-prikaz,
        .shell-volba {
            font-weight: bold;
        }

        .shell-arg {
            font-style: italic;
        }
    """,
    "barevny": """
        .odsazeny {
            margin-left: 4em;
        }

        .shell-prikaz {
            color: #2a2;
            font-weight: bold;
        }

        .shell-volba {
            color: #25d;
            font-weight: bold;
        }

        .shell-arg {
            color: #d43;
        }

        code {
            color: #12a;
        }

        strong {
            font-weight: bold;
            color: #25d;
        }

        em {
            color: #d43;
            font-style: italic;
        }
    """,
    "terminal": """
        html {
            background: #222;
            color: #eee;
            font-family: monospace;
            padding: 1em;
        }

        * {
            line-height: 120%;
            margin: 0;
            padding: 0;
        }

        h1,
        h2 {
            font-size: 1em;
            color: #55f;
        }

        h1 {
            text-transform: uppercase;
        }

        h2 {
            padding-left: 2em;
        }

        p {
            margin-bottom: 1.2em;
            padding-left: 4em;
        }

        .odsazeny {
            padding-left: 6em;
        }

        .shell-prikaz,
        .shell-volba,
        strong {
            color: #55f;
            font-weight: bold;
        }

        .shell-arg,
        em {
            color: #5ff;
        }

        a {
            color: #f5f;
            text-decoration: none;
        }
        a:hover,
        a:focus,
        a:active {
            text-decoration: underline;
        }

        ul {
            margin-left: 4em;
        }

        ul li {
            list-style: none;
            margin-bottom: 1.2em;
        }
        ul li::before {
            content: '· ';
            color: #55f;
            font-style: bold;
        }

        ol {
            margin-left: 4em;
        }

        ol li {
            list-style: decimal inside;
            margin-bottom: 1.2em;
        }
    """,
}
