#!/usr/bin/env python3

from getopt import gnu_getopt as getopt
import sys

from parser import Parser
import css
import formaty
from formaty import html
from formaty import man

if __name__ == "__main__":
    import napoveda

    (opts, args) = (getopt(sys.argv[1:], "f:h", ["help", "format=", "formats", "formaty", "kodovani=", "encoding=", "charset=", "css=", "seznam-css"]));

    zvoleny_format = "text"
    zvolene_kodovani = "utf-8"
    zvolene_css = ""

    for (opt, hodnota) in opts:
        if opt == "--help" or opt == "-h":
            napoveda.zobraz()
            exit(0)
        elif opt == "--format" or opt == "-f":
            zvoleny_format = hodnota.lower()
        elif opt == "--formaty":
            for format in napoveda.formaty:
                print(format)
            exit(0)
        elif opt == "--kodovani" or opt == "--encoding" or opt == "--charset":
            zvolene_kodovani = hodnota
        elif opt == "--css":
            zvolene_css = hodnota
        elif opt == "--seznam-css":
            for styl in css.styly.keys():
                print(styl)
            exit(0)

    if len(args) != 1:
        sys.stderr.write("Neplatný počet vstupních souborů\n")
        napoveda.zobraz()
        exit(1)

    with open(args[0]) as soubor:
        kod = soubor.read()

    parser = Parser()
    vystup_parseru = parser.zpracuj(kod)

    vystup_parseru.kodovani = zvolene_kodovani
    if css.styly.get(zvolene_css) != None:
        vystup_parseru.css = css.styly[zvolene_css]
    else:
        vystup_parseru.css = ""

    if formaty.__dict__.get(zvoleny_format) != None:
        sys.stdout.write(formaty.__dict__[zvoleny_format].preved(vystup_parseru))
    else:
        sys.stderr.write("Formát %s není dostupný.\n" % (zvoleny_format,))
        exit(1)
