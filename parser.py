import re

class Parser:
    def __init__(self):
        pass

    def zpracuj(self, kod):
        manpage = Manpage()
        radky = kod.split("\n")

        while len(radky) > 0:
            radek = radky.pop(0)

            # Metadata
            r = re.fullmatch('\s*%\s*(N[aá]zev|Sekce|Kategorie|Datum)\s*:\s*(.+?)\s*$', radek, flags=re.I)
            if r != None:
                klic = r.groups()[0].lower()
                hodnota = r.groups()[1]

                if klic == 'název' or klic == 'nazev':
                    manpage.nazev = hodnota
                elif klic == 'sekce':
                    manpage.sekce = hodnota
                elif klic == 'kategorie':
                    manpage.kategorie = hodnota
                elif klic == 'datum':
                    manpage.datum = hodnota

                continue

            # Nadpisy s podtržením
            if len(radky) > 0 and radek.strip() != "":
                r = re.fullmatch('\s*([=-])\\1*\s*', radky[0])
                if r != None:
                    uroven = { '=': 1, '-': 2, }[r.groups()[0]]
                    text = radek

                    radky.pop(0)

                    manpage.bloky.append(("nadpis", {
                        "uroven": uroven,
                        "spany": self.zpracuj_spany(text),
                    }))

                    continue

            # Vodorovná čára
            r = re.fullmatch('\s*(?:[*-]\s*){3,}', radek)
            if r != None:
                manpage.bloky.append(("cara", {}))

                continue

            # Nadpisy se znakem '#'
            r = re.fullmatch('\s*(#+)\s*(.+?)\s*', radek)
            if r != None:
                uroven = len(r.groups()[0])
                text = r.groups()[1]

                while radek[-2:] == "  ": # Když pokračuje na dalším řádku
                    radek = radky.pop(0)
                    text = text + "<br>" + radek

                manpage.bloky.append(("nadpis", {
                    "uroven": uroven,
                    "spany": self.zpracuj_spany(text),
                }))

                continue

            # Kód ohraničený ```
            r = re.fullmatch('\s*```\s*([-a-zA-Z0-9#+_*/]*)\s*', radek)
            if r != None:
                jazyk = r.groups()[0]

                text = ""
                while len(radky) > 0 and re.fullmatch('\s*```\s*', radky[0]) == None:
                    text += radky.pop(0) + "\n"

                radky.pop(0) # Odstraň řádek s koncovým ```

                manpage.bloky.append(("kod_blok", {
                    "jazyk": jazyk,
                    "text": text.rstrip(),
                }))

                continue

            # Zápis shellu (řádky uvozené $)
            r = re.fullmatch('\s*\$\s*(.*?)\s*', radek)
            if r != None:
                text = r.groups()[0]

                manpage.bloky.append(("shell", {
                    "text": text,
                }))

                continue

            # Seznamy odrážkové
            r = re.fullmatch('\s*[-*+]\s*(.*?)\s*', radek)
            if r != None:
                text = r.groups()[0]

                while len(radky) > 0 and re.fullmatch('\s*', radky[0]) == None and re.match('#|\s*([-*+]|\d+\\.)', radky[0]) == None:
                    text = text + radky.pop(0)

                manpage.bloky.append(("seznam", {
                    "cislovany": False,
                    "spany": self.zpracuj_spany(text),
                }))

                continue

            # Seznamy číslované
            r = re.fullmatch('\s*[0-9]+\.\s*(.*?)\s*', radek)
            if r != None:
                text = r.groups()[0]

                while len(radky) > 0 and re.fullmatch('\s*', radky[0]) == None and re.match('#|\s*([-*+]|\d+\\.)', radky[0]) == None:
                    text = text + radky.pop(0)

                manpage.bloky.append(("seznam", {
                    "cislovany": True,
                    "spany": self.zpracuj_spany(text),
                }))

                continue

            # Běžné odstavce
            text = radek.strip() + " "
            odsazeny = (re.match('\s', radek) != None)
            while radek.strip() != "":
                if radek[-2:] == "  ":
                    text = text + "<br>"
                radek = radky.pop(0)
                text = text + radek.strip() + " "

            manpage.bloky.append(("odstavec", {
                "spany": self.zpracuj_spany(text),
                "odsazeny": odsazeny,
            }))

        # Odstrannění prázdných bloků
        for (index, (typ, blok)) in enumerate(manpage.bloky):
            if typ == "odstavec" or typ == "seznam" or typ == "nadpis":
                if len(blok["spany"]) == 0:
                    manpage.bloky.pop(index)
                
        return manpage

    def zpracuj_spany(self, kod):
        spany = [('nezpracovany', {'text': kod})]

        while True:
            vse_zpracovane = True
            for (typ, _) in spany:
                if typ == "nezpracovany":
                    vse_zpracovane = False
            if vse_zpracovane:
                break

            for (index, (typ, span)) in enumerate(spany):
                if typ == "nezpracovany":
                    # Escape sekvence
                    r = re.search('\\\\([][\\\\`*_{}()#+-.!$%])', span["text"])
                    if r != None:
                        pred = span["text"][:(r.span()[0])]
                        znak = r.groups()[0]
                        po = span["text"][r.span()[1]:]

                        spany[index] = ("text", {"text": znak, })
                        spany.insert(index + 1, ("nezpracovany", {"text": po, }))
                        spany.insert(index, ("nezpracovany", {"text": pred}))

                        break

                    # HTML tagy nepárové
                    r = re.search('<(br)>', span["text"], flags=re.I)
                    if r != None:
                        pred = span["text"][:(r.span()[0])]
                        tag = r.groups()[0].lower()
                        po = span["text"][r.span()[1]:]
                        
                        if tag == "br":
                            spany[index] = ("zalomeni", {})

                        spany.insert(index + 1, ("nezpracovany", {"text": po, }))
                        spany.insert(index, ("nezpracovany", {"text": pred}))

                        break

                    # Odkazy se špičatou závorkou (<>)
                    r = re.search('<(.*?)>', span["text"])
                    if r != None:
                        pred = span["text"][:(r.span()[0])]
                        po = span["text"][r.span()[1]:]
                        cil = r.groups()[0]

                        spany[index] = ("odkaz", {
                            "spany": [("text", {"text": cil})],
                            "cil": cil,
                        })
                        spany.insert(index + 1, ("nezpracovany", {"text": po, }))
                        spany.insert(index, ("nezpracovany", {"text": pred}))

                        break

                    # Markdown odkazy (ne reference)
                    r = re.search('\[(.*?)\]\((.*?)\)', span["text"])
                    if r != None:
                        pred = span["text"][:(r.span()[0])]
                        po = span["text"][r.span()[1]:]
                        cil = r.groups()[1]
                        popisek = r.groups()[0]

                        spany[index] = ("odkaz", {
                            "spany": self.zpracuj_spany(popisek),
                            "cil": cil,
                        })
                        spany.insert(index + 1, ("nezpracovany", {"text": po, }))
                        spany.insert(index, ("nezpracovany", {"text": pred}))

                        break

                    # Kód se dvěma backticky (``)
                    r = re.search('``\s*((?:(?!``).)+)\s*``', span["text"])
                    if r != None:
                        pred = span["text"][:(r.span()[0])]
                        text = r.groups()[0]
                        po = span["text"][r.span()[1]:]

                        spany[index] = ("kod", {"text": text, })
                        spany.insert(index + 1, ("nezpracovany", {"text": po, }))
                        spany.insert(index, ("nezpracovany", {"text": pred}))

                        break

                    # Kód s jedním backtickem (`)
                    r = re.search('`\s*(.*?)\s*`', span["text"])
                    if r != None:
                        pred = span["text"][:(r.span()[0])]
                        text = r.groups()[0]
                        po = span["text"][r.span()[1]:]

                        spany[index] = ("kod", {"text": text, })
                        spany.insert(index + 1, ("nezpracovany", {"text": po, }))
                        spany.insert(index, ("nezpracovany", {"text": pred}))

                        break

                    # Zdůraznění
                    r = re.search('(\*{1,2})(.*?)\\1', span["text"])
                    if r != None:
                        pred = span["text"][:(r.span()[0])]
                        znacka = r.groups()[0]
                        text = r.groups()[1]
                        po = span["text"][r.span()[1]:]

                        spany[index] = ("zdurazneni", {
                            "spany": self.zpracuj_spany(text),
                            "silne": (znacka == "**"),
                        })
                        spany.insert(index + 1, ("nezpracovany", {"text": po, }))
                        spany.insert(index, ("nezpracovany", {"text": pred}))

                        break

                    # Prostý text
                    if re.search('\S', span["text"]) != None:
                        text = re.sub('\s+', ' ', span["text"])
                        spany[index] = ("text", {"text": text })
                    else:
                        spany.pop(index)

        return spany

class Manpage:
    nazev = None
    sekce = None
    kategorie = None
    datum = None

    bloky = []

