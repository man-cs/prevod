# Program pro převod formátu [man-cs](https://gitlab.com/man-cs) do jiných formátů

Tento program převede formát používaný v [man-cs](https://gitlab.com/man-cs) do jiných formátů.

Podporované výstupní formáty:

  *    HTML
  *    man (nekomprimovaná manuálová stránka)

## Použití

Ve všech následujících ukázkách se předpokládá, že je program uložen v adresáři `prevod/` a pracovním aresářem je nadřazený adresář.

Převod na manuálovou stránku (výstup je uložen do `./cat.1.mancs`):

    python prevod/ --format=man ./man-cs/manpages/cat/cat.1.mancs > ./cat.1.mancs

Převod do HTML (výstu je uložen do `./cat.1.html`):

    python prevod/ --format=html ./man-cs/manpages/cat/cat.1.mancs > ./cat.1.html