import re

def preved(manpage):
    vystup = """
    <!doctype html>
    <html>
        <head>
            <meta charset="{manpage.kodovani}">
            <title>{manpage.nazev}</title>
            <style>{manpage.css}</style>
        </head>
        <body>
    """.format(manpage=manpage);

    while len(manpage.bloky) > 0:
        (typ, blok) = manpage.bloky.pop(0)

        if typ == "nadpis":
            uroven = min(6, max(0, blok["uroven"]))
            vystup = vystup + "<h{0}>{1}</h{0}>".format(uroven, spany_na_html(blok["spany"]))
        elif typ == "odstavec":
            if blok["odsazeny"]:
                vystup = vystup + '<p class="odsazeny">{}</p>'.format(spany_na_html(blok["spany"]))
            else:
                vystup = vystup + "<p>{}</p>".format(spany_na_html(blok["spany"]))
        elif typ == "kod_blok":
            vystup = vystup + '<pre data-lang="{jazyk}"><code data-lang="{jazyk}">{}</code></pre>'.format(entity(blok["text"]), jazyk = entity(blok["jazyk"]))
        elif typ == "seznam":
            cislovany = blok["cislovany"]
            polozky = [blok["spany"]]

            while len(manpage.bloky) > 0 and manpage.bloky[0][0] == typ and manpage.bloky[0][1]["cislovany"] == cislovany:
                polozky.append(manpage.bloky.pop(0)[1]["spany"])

            tag = cislovany and "ol" or "ul"

            vystup = vystup + "<{}>".format(tag)

            for spany in polozky:
                if len(spany) > 0:
                    vystup = vystup + "<li>{}</li>".format(spany_na_html(spany))

            vystup = vystup + "</{}>".format(tag)
        elif typ == "shell":
            vystup = vystup + '<p data-lang="shell">{}</p>'.format(html_shell(blok["text"]))
        elif typ == "cara":
            vystup = vystup + "<hr>"

    vystup = vystup + "</body></html>"

    return vystup

def spany_na_html(spany):
    html = ""

    for (typ, span) in spany:
        if typ == "text":
            html = html + entity(span["text"])
        elif typ == "kod":
            html = html + "<code>" + entity(span["text"]) + "</code>"
        elif typ == "zdurazneni":
            html = html + "<{tag}>{html}</{tag}>".format(html = spany_na_html(span["spany"]), tag = (span["silne"] and "strong" or "em"))
        elif typ == "zalomeni":
            html = html + "<br>"
        elif typ == "odkaz":
            html = html + '<a href="{cil}">{text}</a>'.format(cil=entity(span["cil"]), text=spany_na_html(span["spany"]))

    return html

def entity(text):
    if len(text) == 1:
        if text == "<":
            return "&lt;"
        elif text == "&":
            return "&amp;"
        elif text == '"':
            return "&quot;"
        else:
            return text
    return re.sub('([&<"])', lambda m: entity(m.groups()[0]), text)

def html_shell(text):
    def html_shell_part(text):
        if re.fullmatch('-\S[a-zA-Z0-9_-]*', text) != None:
            return '<span class="shell-volba">{}</span>'.format(entity(text))
        elif re.fullmatch('\w+', text) != None:
            return '<span class="shell-arg">{}</span>'.format(entity(text))
        else:
            return re.sub('(-\S[a-zA-Z0-9_-]*|\w+)', lambda m: html_shell_part(m.groups()[0]), text)

    r = re.fullmatch('(\w*)(.*?)', text)
    return '<span class="shell-prikaz">{}</span>{}'.format(entity(r.groups()[0]), html_shell_part(r.groups()[1]))
