import re

def preved(manpage):
    vystup = ""

    # Hlavička
    vystup = vystup + '.TH "{nazev}" "{sekce}" "{datum}" "" "{kategorie}"\n'.format(
        nazev = (manpage.nazev != None) and groff_escape(manpage.nazev) or "",
        sekce = (manpage.sekce != None) and groff_escape(manpage.sekce) or "",
        datum = (manpage.datum != None) and groff_escape(manpage.datum) or "",
        kategorie = (manpage.kategorie != None) and groff_escape(manpage.kategorie) or "",
    )

    cislovani_pozice = 1

    # Samotné zpracování
    for (typ, blok) in manpage.bloky:
        # Reset pozice v seznamu
        if not (typ == "seznam" and blok["cislovany"]):
            cislovani_pozice = 1

        if typ == "nadpis":
            if blok["uroven"] == 1:
                vystup = vystup + '.SH "{}"\n'.format(groff_escape(spany_na_text(blok["spany"]).upper()))
            else:
                vystup = vystup + '.SS\n{}\n'.format(groff_escape(spany_na_text(blok["spany"])))

        elif typ == "odstavec":
            if blok["odsazeny"]:
                vystup = vystup + ".RS 4\n"
            else:
                vystup = vystup + ".RE\n"
            vystup = vystup + groff_odstavec(blok["spany"], odsazeni = (blok["odsazeny"] and 4 or 0)) + "\n\n"
            if blok["odsazeny"]:
                vystup = vystup + ".RE\n"

        elif typ == "kod_blok":
            vystup = vystup + ".RS 2\n" + groff_escape(blok["text"]).replace("\n", "\n.RE\n.RS 2\n") + "\n.RE\n\n";

        elif typ == "cara":
            vystup = vystup + ".ce\n\\fB*  *  *\\fP\n\n"

        elif typ == "seznam" and blok["cislovany"]:
            vystup = vystup + ".IP \\fB{}.\\fP\n{}\n\n".format(cislovani_pozice, groff_odstavec(blok["spany"]))
            cislovani_pozice = cislovani_pozice + 1

        elif typ == "seznam" and not blok["cislovany"]:
            vystup = vystup + ".IP \\fB\(bu\\fP\n{}\n\n".format(groff_odstavec(blok["spany"]))

        elif typ == "shell":
            vystup = vystup + ".RE\n" + groff_shell(blok["text"].strip()) + "\n"

    return vystup

def groff_escape(text):
    return re.sub("\\.", "\[char46]", re.sub("\\\\", "\[char92]", text))

def spany_na_text(spany, zalomeni = ""):
    text = ""

    for (typ, span) in spany:
        if typ == "text" or typ == "kod":
            text = text + span["text"]
        elif typ == "zalomeni":
            text = text + zalomeni
        elif typ == "zdurazneni":
            text = text + spany_na_text(span["spany"])

    return text

def groff_odstavec(spany, pred = "", po = "", odsazeni = 0):
    vystup = ""

    for (typ, span) in spany:
        if typ == "text":
            vystup = vystup + pred + span["text"] + po;
        elif typ == "zdurazneni" and span["silne"]:
            vystup = vystup + groff_odstavec(span["spany"], "\\fB", "\\fP");
        elif typ == "zdurazneni" and not span["silne"]:
            vystup = vystup + groff_odstavec(span["spany"], "\\fI", "\\fP");
        elif typ == "zalomeni":
            vystup = vystup + "\n.RE\n.RS {odsazeni}\n".format(odsazeni = odsazeni)
        elif typ == "odkaz" and span["cil"] == spany_na_text(span["spany"]):
            vystup = vystup + groff_odstavec(span["spany"], "\\fI", "\\fP", odsazeni)
        elif typ == "odkaz":
            vystup = vystup + groff_odstavec(span["spany"], "\\fI", "\\fP", odsazeni) + " (" + groff_escape(span["cil"]) + ")"
        elif typ == "kod":
            vystup = vystup + "\\fB{}\\fP".format(span["text"])

    return vystup

def groff_shell(text):
    def groff_shell_part(text):
        if re.fullmatch('-\S[a-zA-Z0-9_-]*', text) != None:
            return "\\fB{}\\fP".format(groff_escape(text))
        elif re.fullmatch('\w+', text) != None:
            return "\\fI{}\\fP".format(groff_escape(text))
        else:
            return re.sub('(-\S[a-zA-Z0-9_-]*|\w+)', lambda m: groff_shell_part(m.groups()[0]), text)

    r = re.fullmatch('(\w*)(.*?)', text)
    return "\\fB{}\\fP{}".format(r.groups()[0], groff_shell_part(r.groups()[1]))
